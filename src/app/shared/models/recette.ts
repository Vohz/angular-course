export interface Recette {
    id: number,
    nom: string,
    temps_cuisson: string,
    image: string
}
